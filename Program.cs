﻿using System;
using Newtonsoft.Json.Linq;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("This is a test for .NET Core under Linux");

            Console.WriteLine("Test Json.NET");
            var json = "{ \"name\": \"test\", \"score\": 9.5 }";
            Console.WriteLine(json);
            var jobj = JObject.Parse(json);
            var jname = jobj.Value<string>("name");
            var jscore = jobj.Value<double>("score");
            Console.WriteLine($"name: {jname}, score: {jscore}");
        }
    }
}
